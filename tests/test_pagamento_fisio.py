# test_pagamento_fisio.py

from humaniza import pagamento_fisio


def test_calcular_somatorio():
    horas_por_dia = [5, 5, 5, 5, 5]
    resultado = pagamento_fisio.calcular_somatorio(horas_por_dia)
    assert resultado == 650.00  # 5 * 5 * 26


def test_obter_valor_aulas_experimentais(monkeypatch):
    inputs = iter(["sim", "2", "100", "200"])

    monkeypatch.setattr("builtins.input", lambda _: next(inputs))
    resultado = pagamento_fisio.obter_valor_aulas_experimentais()
    assert resultado == 30.00  # 0.10 * (100 + 200)


def test_obter_ajuda_de_custo(monkeypatch):
    inputs = iter(["20.0"])

    monkeypatch.setattr("builtins.input", lambda _: next(inputs))
    resultado = pagamento_fisio.obter_ajuda_de_custo(5)
    assert resultado == 100.0  # 5 * 20.0


def test_calcular_pagamento_total(monkeypatch):
    horas_por_dia = [5, 5, 5, 5, 5]
    num_dias = 5

    inputs = iter(["sim", "2", "100", "200", "20.0"])

    monkeypatch.setattr("builtins.input", lambda _: next(inputs))
    resultado = pagamento_fisio.calcular_pagamento_total(horas_por_dia, num_dias)
    assert (
        resultado == 780.00
    )  # 650.00 (horas) + 30.00 (aulas experimentais) + 100.00 (ajuda de custo)


def test_obter_dados_fisio(monkeypatch):
    fisioterapeutas = []

    def mock_input(prompt):
        responses = {
            "Digite o número de fisioterapeutas: ": "1",
            "Digite o nome da fisioterapeuta: ": "Fisioterapeuta A",
            "Digite a quantidade de dias para Fisioterapeuta A: ": "2",
            "Digite a quantidade de horas para o dia 1 de Fisioterapeuta A: ": "5",
            "Digite a quantidade de horas para o dia 2 de Fisioterapeuta A: ": "4",
            "Houve aulas experimentais? (sim/não): ": "não",
            "Digite o valor da ajuda de custo por dia: ": "10.0",
        }
        return responses[prompt]

    monkeypatch.setattr("builtins.input", mock_input)
    monkeypatch.setattr("humaniza.pagamento_fisio.fisioterapeutas", fisioterapeutas)

    pagamento_fisio.obter_dados_fisio()

    assert len(fisioterapeutas) == 1
    assert fisioterapeutas[0]["nome"] == "Fisioterapeuta A"
    assert fisioterapeutas[0]["pagamento_total"] == 254.0  # (5+4)*26 + 2*10


def test_total_pg_fisio(monkeypatch):
    fisioterapeutas = [
        {"nome": "Fisioterapeuta A", "pagamento_total": 260.0},
        {"nome": "Fisioterapeuta B", "pagamento_total": 300.0},
    ]

    monkeypatch.setattr("humaniza.pagamento_fisio.fisioterapeutas", fisioterapeutas)
    resultado = pagamento_fisio.total_pg_fisio()
    assert resultado == 560.0  # 260 + 300

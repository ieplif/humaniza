# test_pagamento_geral.py

from humaniza import pagamento_geral
from humaniza.pagamento_geral import Despesa


def test_adicionar_despesa(monkeypatch):
    despesas = []

    def mock_input(prompt):
        responses = {
            "Digite a descrição da despesa: ": "Aluguel",
            "Digite a data da despesa (formato DD/MM/YYYY): ": "01/05/2023",
            "Digite o valor da despesa: ": "1500.0",
        }
        return responses[prompt]

    monkeypatch.setattr("builtins.input", mock_input)
    monkeypatch.setattr("humaniza.pagamento_geral.despesas", despesas)

    pagamento_geral.adicionar_despesa()

    assert len(despesas) == 1
    assert despesas[0].descricao == "Aluguel"
    assert despesas[0].data == "01/05/2023"
    assert despesas[0].valor == 1500.0


def test_calcular_total_despesas(monkeypatch):
    despesas = [
        Despesa("Aluguel", "01/05/2023", 1500.0),
        Despesa("Internet", "02/05/2023", 100.0),
    ]

    monkeypatch.setattr("humaniza.pagamento_geral.despesas", despesas)
    resultado = pagamento_geral.calcular_total_despesas()
    assert resultado == 1600.0  # 1500 + 100


def test_calcular_total_despesas_por_mes(monkeypatch):
    despesas = [
        Despesa("Aluguel", "01/05/2023", 1500.0),
        Despesa("Internet", "02/05/2023", 100.0),
        Despesa("Energia", "15/06/2023", 200.0),
    ]

    monkeypatch.setattr("humaniza.pagamento_geral.despesas", despesas)
    resultado = pagamento_geral.calcular_total_despesas_por_mes()
    assert resultado == {"05-2023": 1600.0, "06-2023": 200.0}


def test_obter_desp_geral(monkeypatch):
    despesas = []

    def mock_input(prompt):
        responses = {
            "Deseja adicionar uma despesa? (sim/não): ": "não",
        }
        return responses[prompt]

    monkeypatch.setattr("builtins.input", mock_input)
    monkeypatch.setattr("humaniza.pagamento_geral.despesas", despesas)

    pagamento_geral.obter_desp_geral()

    assert len(despesas) == 0  # No expenses added


def test_total(monkeypatch):
    despesas = [
        Despesa("Aluguel", "01/05/2023", 1500.0),
        Despesa("Internet", "02/05/2023", 100.0),
        Despesa("Energia", "15/06/2023", 200.0),
    ]

    def mock_total_pg_fisio():
        return 500.0

    monkeypatch.setattr("humaniza.pagamento_fisio.total_pg_fisio", mock_total_pg_fisio)
    monkeypatch.setattr("humaniza.pagamento_geral.despesas", despesas)

    # Capturing print statements
    import builtins

    print_statements = []

    def mock_print(s=""):
        print_statements.append(s)

    monkeypatch.setattr(builtins, "print", mock_print)

    pagamento_geral.total()

    # Verificar as saídas
    expected_output = [
        "Total de despesas gerais: R$1800.00",
        "",
        "Total de despesas gerais por mês:",
        "05-2023: R$1600.00",
        "06-2023: R$200.00",
        "Total de pagamentos para todas as fisioterapeutas: R$500.00",
        "Total de DESPESASs: R$2300.00",
    ]

    for idx, expected in enumerate(expected_output):
        assert print_statements[idx] == expected

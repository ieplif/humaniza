# test_receita_clinica.py

from humaniza import receita_clinica
from humaniza.receita_clinica import Receita


def test_adicionar_receita(monkeypatch):
    receitas = []

    def mock_input(prompt):
        responses = {
            "Digite a data da receita (formato DD/MM/YYYY): ": "01/05/2023",
            "Digite o nome do paciente: ": "Paciente 1",
            "Digite a atividade realizada: ": "Consulta",
            "Digite o valor da receita: ": "1000.0",
        }
        return responses[prompt]

    monkeypatch.setattr("builtins.input", mock_input)
    monkeypatch.setattr("humaniza.receita_clinica.receitas", receitas)

    receita_clinica.adicionar_receita()

    assert len(receitas) == 1
    assert receitas[0].data == "01/05/2023"
    assert receitas[0].nome_paciente == "Paciente 1"
    assert receitas[0].atividade == "Consulta"
    assert receitas[0].valor == 1000.0


def test_calcular_total_receitas_por_mes(monkeypatch):
    receitas = [
        Receita("01/05/2023", "Paciente 1", "Consulta", 1000.0),
        Receita("15/05/2023", "Paciente 2", "Fisioterapia", 2000.0),
        Receita("01/06/2023", "Paciente 3", "Consulta", 1500.0),
    ]

    monkeypatch.setattr("humaniza.receita_clinica.receitas", receitas)
    resultado = receita_clinica.calcular_total_receitas_por_mes()
    assert resultado == {"05-2023": 3000.0, "06-2023": 1500.0}


def test_calcular_receita_total(monkeypatch):
    receitas = [
        Receita("01/05/2023", "Paciente 1", "Consulta", 1000.0),
        Receita("15/05/2023", "Paciente 2", "Fisioterapia", 2000.0),
    ]

    monkeypatch.setattr("humaniza.receita_clinica.receitas", receitas)
    resultado = receita_clinica.calcular_receita_total()
    assert resultado == 3000.0  # 1000 + 2000


def test_obter_receita(monkeypatch):
    receitas = []

    def mock_input(prompt):
        responses = {
            "Deseja adicionar uma receita? (sim/não): ": "não",
        }
        return responses[prompt]

    monkeypatch.setattr("builtins.input", mock_input)
    monkeypatch.setattr("humaniza.receita_clinica.receitas", receitas)

    receita_clinica.obter_receita()

    assert len(receitas) == 0  # No recipes added


def test_exibir_receita(monkeypatch):
    receitas = [
        Receita("01/05/2023", "Paciente 1", "Consulta", 1000.0),
        Receita("15/05/2023", "Paciente 2", "Fisioterapia", 2000.0),
        Receita("01/06/2023", "Paciente 3", "Consulta", 1500.0),
    ]

    monkeypatch.setattr("humaniza.receita_clinica.receitas", receitas)

    # Capturing print statements
    import builtins

    print_statements = []
    monkeypatch.setattr(builtins, "print", lambda s: print_statements.append(s))

    receita_clinica.exibir_receita()

    assert "Total de receitas por mês:" in print_statements
    assert "05-2023: R$3000.00" in print_statements
    assert "06-2023: R$1500.00" in print_statements
    assert "Total de Receitas: R$4500.00" in print_statements
